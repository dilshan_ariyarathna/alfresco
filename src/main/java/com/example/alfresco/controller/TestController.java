package com.example.alfresco.controller;

import com.example.alfresco.Service.CmisService;
import jakarta.annotation.Resource;
import org.apache.chemistry.opencmis.client.api.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static org.apache.chemistry.opencmis.commons.enums.BaseTypeId.CMIS_FOLDER;

@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private CmisService cmisService;

    @PostMapping("/add")
    public String add(){
        Folder folder = cmisService.getRootFolder();
        Document docA = cmisService.createDocument(cmisService.getRootFolder(), "document-a.txt");
        Document docB = cmisService.createDocument(cmisService.getRootFolder(), "document-b.txt");

        return "Success";
    }

    @PostMapping("/remove")
    public String remove(){
        Document docA = cmisService.createDocument(cmisService.getRootFolder(), "document-a.txt");
        Document docB = cmisService.createDocument(cmisService.getRootFolder(), "document-b.txt");

        cmisService.remove(docA);
        cmisService.remove(docB);
        return "Success";
    }

    @PostMapping("/addAndRemove")
    public void addAndRemove(){
        AddAndRemoveFiles();
    }

    private void AddAndRemoveFiles(){

//        Document docA = cmisService.createDocument(cmisService.getRootFolder(), "document-a.txt");
//        Document docB = cmisService.createDocument(cmisService.getRootFolder(), "document-b.txt");

//        cmisService.addAspect(docA, "P:cmisassoc:relationable");
//        cmisService.addAspect(docB, "P:cmisassoc:relationable");

//        ObjectId relationship = cmisService.createRelationship(docA, docB, "R:cmisassoc:related");
//        ItemIterable<Relationship> relationships = cmisService.getRelationships(docA, "R:cmisassoc:related");
//        relationships.forEach((r) -> {
//        });

//        Map<String, Object> properties = new HashMap<>();
//        properties.put("cmisassoc:relatedRef", Arrays.asList(new String[]{docB.getProperty("alfcmis:nodeRef").getValueAsString()}));
//        cmisService.updateProperties(docA, properties);
//
//        ItemIterable<QueryResult> queryRelationships =
//                cmisService.query("SELECT cmisassoc:relatedRef FROM cmisassoc:relationable WHERE cmis:name='document-a.txt'");
//        queryRelationships.forEach((r) -> {
//        });

        String query = "SELECT * FROM cmis:folder WHERE CONTAINS('PATH:\"//app:company_home/app:dictionary/*\"')";

        ItemIterable<QueryResult> objectList = cmisService.query(query);
        int i = 0;

//        cmisService.remove(docA);
//        cmisService.remove(docB);
    }
}
